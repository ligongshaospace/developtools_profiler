/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SMARTPERF_COMMAND_H
#define SMARTPERF_COMMAND_H

#include <iostream>
#include <vector>
#include "common.h"

namespace OHOS {
namespace SmartPerf {
class SmartPerfCommand {
public:
    const std::string smartPerfExeName = "SP_daemon";
    const std::string smartPerfVersion = "1.0.2\n";
    const std::string smartPerfMsgErr = "error input!\n use command '--help' get more information\n";
    const std::string smartPerfMsg = "OpenHarmony performance testing tool SmartPerf command-line version\n"
        "Usage: SP_daemon [options] [arguments]\n\n"
        "options:\n"
        " -N             set the collection times(default value is 0) range[1,2147483647], for example: -N 10 \n"
        " -PKG           set package name, must add, for example: -PKG ohos.samples.ecg \n"
        " -c             get device CPU frequency and CPU usage, process CPU usage and CPU load .. \n"
        " -g             get device GPU frequency and GPU load  \n"
        " -f             get app refresh fps(frames per second) and fps jitters \n"
        " -profilerfps   get refresh fps and timestamp \n"
        " -t             get remaining battery power and temperature.. \n"
        " -p             get battery power consumption and voltage \n"
        " -r             get process memory and total memory .. \n"
        " -snapshot      get screen capture\n"
        " -net           get uplink and downlink traffic\n"
        " -start         collection start command \n"
        " -stop          collection stop command \n"
        " -VIEW          set layler, for example: -VIEW DisplayNode \n"
        " -screen        get screen resolution \n"
        " -editor        editing function. performance testing, marking characters"
        " -OUT           set csv output path.\n"
        " -d             get device DDR information\n\n"
        "example:\n"
        "SP_daemon -N 20 -c -g -t -p -r -net -snapshot \n"
        "SP_daemon -N 20 -PKG ohos.samples.ecg -c -g -t -p -f -r -net -snapshot \n"
        "SP_daemon -start -c \n"
        "SP_daemon -stop \n"
        "SP_daemon -screen \n";

    const int oneParam = 1;
    const int twoParam = 2;
    const int threeParamMore = 3;
    explicit SmartPerfCommand(std::vector<std::string> argv);
    ~SmartPerfCommand() {};
    static void InitSomething();
    std::string ExecCommand();
    void HelpCommand(CommandHelp type) const;
    void HandleCommand(std::string argStr, std::string argStr1);
    // 采集次数
    int num = 0;
    // 包名
    std::string pkgName = "";
    // 图层名
    std::string layerName = "";
    // 是否开启trace 抓取
    int trace = 0;
    // csv输出路径
    std::string outPath = "/data/local/tmp/data.csv";
    std::string outPathParam = "";
    // 指定进程pid
    std::string pid = "";
    // 采集配置项
    std::vector<std::string> configs;
};
}
}
#endif // SMARTPERF_COMMAND_H