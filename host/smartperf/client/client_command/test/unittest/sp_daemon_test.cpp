/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <thread>

#include <gtest/gtest.h>

#include "sp_utils.h"

using namespace testing::ext;
using namespace std;

namespace OHOS {
namespace SmartPerf {
class SPdaemonTest : public testing::Test {
public:
    static void SetUpTestCase() {}
    static void TearDownTestCase() {}

    void SetUp() {}
    void TearDown() {}
};

/**
 * @tc.name: CpuTestCase
 * @tc.desc: Test CPU
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, CpuTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -N 1 -c";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("cpu0Usage");
    std::string::size_type strTwo = result.find("cpu0idleUsage");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: GpuTestCase
 * @tc.desc: Test GPU
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, GpuTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -N 1 -g";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("gpuFrequency");
    std::string::size_type strTwo = result.find("gpuLoad");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: FpsTestCase
 * @tc.desc: Test FPS
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, FpsTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -N 1 -PKG ohos.samples.ecg -f";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("fpsJitters");
    std::string::size_type strTwo = result.find("timestamp");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: TemperatureTestCase
 * @tc.desc: Test Temperature
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, TemperatureTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -N 1 -t";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("Battery");
    std::string::size_type strTwo = result.find("shell_back");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: PowerTestCase
 * @tc.desc: Test Power
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, PowerTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -N 1 -p";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("currentNow");
    std::string::size_type strTwo = result.find("voltageNow");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: RamTestCase
 * @tc.desc: Test RAM
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, RamTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -N 1 -r";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("memAvailable");
    std::string::size_type strTwo = result.find("memTotal");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: SnapShotTestCase
 * @tc.desc: Test SnapShot
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, SnapShotTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -N 1 -snapshot";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("capture");
    std::string::size_type strTwo = result.find(".png");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: NetWorkTestCase
 * @tc.desc: Test NetWork
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, NetWorkTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -N 1 -net";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("networkDown");
    std::string::size_type strTwo = result.find("networkUp");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: StartTestCase
 * @tc.desc: Test Start
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, StartTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -start -c";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("Collection");
    std::string::size_type strTwo = result.find("begins");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: StopTestCase
 * @tc.desc: Test Stop
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, StopTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -stop";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("Collection");
    std::string::size_type strTwo = result.find("ended");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: ProfilerFpsTestCase
 * @tc.desc: Test ProfilerFps
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, ProfilerFpsTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -profilerfps 10";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("set");
    std::string::size_type strTwo = result.find("success");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: ScreenTestCase
 * @tc.desc: Test Screen
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, ScreenTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -screen";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("activeMode");
    std::string::size_type strTwo = result.find("refreshrate");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}

/**
 * @tc.name: FrameLossTestCase
 * @tc.desc: Test FrameLoss
 * @tc.type: FUNC
 */
HWTEST_F(SPdaemonTest, FrameLossTestCase, TestSize.Level1)
{
    std::string cmd = "SP_daemon -editor frameLoss";
    std::string result = "";
    bool flag = false;
    auto ret = SPUtils::LoadCmd(cmd, result);
    std::string::size_type strOne = result.find("BUNDLE_NAME");
    std::string::size_type strTwo = result.find("TOTAL_APP_MISSED_FRAMES");
    if ((strOne != result.npos) && (strTwo != result.npos)) {
        flag = true;
    }

    EXPECT_EQ(ret, true);
    EXPECT_EQ(flag, true);
}
} // namespace OHOS
} // namespace SmartPerf